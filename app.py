import json
from flask import Flask, request

from functions.user_create import user_create

app = Flask(__name__)


@app.route("/create", methods=["POST"])
def create():
    return user_create(json.loads(request.data))