import re
import pydantic
from typing import List, Optional

from utils.exception_handle import EmailValidationError
 

class User(pydantic.BaseModel):
    name: str
    age: int
    email: str
    phone: str
    address: Optional[str]

    @pydantic.validator("email")
    @classmethod
    def email_valid_check(cls, email) -> None:
        """Validator to check whether email is valid"""
        # Make a regular expression for validating an Email
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if(re.fullmatch(regex, email)):
            return email
        else:
            message = "Given email address ({0}) is not valid.".format(email)
            raise EmailValidationError(email=email, message=message)
