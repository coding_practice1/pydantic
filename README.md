
# Pydantic Practice

pydantic enforces type hints at runtime, and provides user friendly errors when data is invalid.


## Acknowledgements

 - [Pydantic Documentation](https://pydantic-docs.helpmanual.io/)
 

## API Reference

#### POST User Information

```http
  POST /create
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `name` | `string` | **Required**.|
| `age` | `int` | **Required**.|
| `email` | `string` | **Required**.|
| `phone` | `string` | **Required**.|
| `address` | `string` | **Optional**.|



## Authors

- [@ujjawalpoudel](https://gitlab.com/ujjawalpoudel)


## License

[MIT](https://choosealicense.com/licenses/mit/)


## Tech Stack


**Server:** Python

